package com.example.user.mycatchball;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class ResultView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_view);

        TextView scoreTag = (TextView) findViewById(R.id.scoreTag);
        TextView highscoreTag = (TextView) findViewById(R.id.highscoreTag);

        int score = getIntent().getIntExtra("SCORE", 0);
        scoreTag.setText(score + "");

        SharedPreferences settings = getSharedPreferences("HIGH SCORE", Context.MODE_PRIVATE);
        int highscore = settings.getInt("HIGH_SCORE", 0);

        if(score > highscore){
            highscoreTag.setText("High Score: " + score );

            //Update the high score
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("HIGH_SCORE", score);
            editor.commit();
        } else{
            highscoreTag.setText("High Score: "+ highscore);
        }

    }

    public void tryAgain(View view){

        startActivity(new Intent(getApplicationContext(), MainActivity.class));

    }

    public void mainMenu(View view){
        startActivity(new Intent(getApplicationContext(), MainMenuView.class));
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event){
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch (event.getKeyCode()){
                case KeyEvent.KEYCODE_BACK:
                    return true;
            }
        }

        return super.dispatchKeyEvent(event);
    }
}

